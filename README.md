AIRBNB strategy ultrafast strategy

    1. Como o curto tempo para fazer o modelamente, eu escolhi uma estrategia de Multiple Model Adaptive Estimation with supervised manual classification.

a) Manual classification com as variaveis de Room type, e ETL de texto para detetar a clase de imovel(apto/casa,etc)

b) Graficar dados entre variaveis para cada level/type e detetar best linear correlation

c) group levels by similarity


   2. A função de custo usada foi MSE, para regressão dos modelos parciais de cada level(room type & property type) 

   3. O performance dos modelos parciais foi evaluado com MSE, usando um framework automatizado(mlr3) para escolher o melhor algoritmo entre linear, SVM, Random Forest, XGboost, etc.

     O grande volume de datos permite implementar um MMAE muito robusto e o performance da exatidão/erro total = performance dos erros dos modelos parciais.

  4. O indicador de minimo MSE foi usado para escolher modelos de maior performance, o modelo mais escolhido foi o Random forest.

  5. Para validar o performance com maior seguridade, em cada modelo parcial foi graficado o dado calculado por o modelo vs o dado real inicial da base de dados.

     O modelo pode melhorar adicionando outlier filtering, e dados da variavel "amenities", pois por o limite de tempo  a modelagem foi construida com os dados menos complexos de extrair.

 
 
